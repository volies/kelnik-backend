<?php 

$jsonFile = file_get_contents( __DIR__ . DIRECTORY_SEPARATOR . 'products.json' );
$jsonData = json_decode($jsonFile);

$page = $_POST['page'];
$perPage = $_POST['perPage'];
$orderBy = $_POST['orderBy'];
$productsAll = $jsonData->products;

// Sorting
if ( $orderBy == "price" ) {
	usort($productsAll, "cmpPrice");
}
elseif ( $orderBy == "rooms" ) {
	usort($productsAll, "cmpRooms");
}
function cmpPrice($a, $b) { return $a->price > $b->price; };
function cmpRooms($a, $b) { return $a->rooms > $b->rooms; }

$products = array_slice($productsAll, ($page - 1) * $perPage, $perPage);
if ( !$products ) return false;

$html_products = "";
foreach ($products as $product) {
	$class_elected = "";
	if ( $product->elected ){
		$class_elected = "product__elected-selected";
	}

	$sale = "";
	if ( isset($product->sale) && $product->sale ) {
		foreach ($product->sale as $sale_val) {
			$sale .= "<span>$sale_val</span>";
		}
	}

	$status_slug = $product->status[0];
	$status_text = $product->status[1];

	$price = number_format($product->price, 0, '.', ' ');

	$html_products .= "
		<div class='product-wrap'>
			<article class='product product-$status_slug'>
				<span class='product__elected $class_elected to-elected'></span>
				<div class='product__sale'>$sale</div>
				<div class='product__content'>
					<div class='product__img'>
						<img src='$product->image' alt=''>
					</div>
					<h2 class='product__name'>$product->name</h2>
					<div class='product__prop'>
						<p class='product__decor'>$product->decor</p>
						<p class='product__prop-value'><span>$product->area м<sup>2</sup></span><br />площадь</p>
						<p class='product__prop-value'><span>$product->floor</span><br />этаж</p>
					</div>
					<p class='product__price'>$price руб.</p>
				</div>
				<div class='product__status'>
					<p>$status_text</p>
				</div>
			</article>
		</div>
	";
}

echo $html_products; 
