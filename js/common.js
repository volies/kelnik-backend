$(document).ready(function(){
	// Navigation
	(function(){
		var hamburger = document.getElementById('hamburger'),
		closeNavigation = document.getElementById('navigation-close'),
		navigation = document.getElementById('navigation');

		hamburger.onclick = function(e){
			e.preventDefault();
			navigation.style.display = 'block';
			document.body.className += ' overflow-hidden';
		};
		closeNavigation.onclick = function(e){
			e.preventDefault();
			navigation.style.display = 'none';
			document.body.classList.remove('overflow-hidden');
		}

		document.onclick = function(e){
			if ( (!$(navigation).is(e.target) && $(navigation).has(e.target).length === 0 ) &&
				(!$(hamburger).is(e.target) && $(hamburger).has(e.target).length === 0 ) ) {
				if ( window.innerWidth <= 992 ) {
					navigation.style.display = 'none';
				}
			}
		};

		$(window).resize(function(){
			if ( window.innerWidth > 992 ) {
				$(navigation).attr('style','');
				document.body.classList.remove('overflow-hidden');
			}
		});

	}());

	// Add product elected
	$('.products').on('click', '.to-elected', function(e){
		$(this).toggleClass('product__elected-selected');
	});

	// To-top
	(function(){
		var top = $('.on-to-top');
		$(window).scroll(function(){
			checkToTop();
		});

		top.click(function(){
			$("html, body").animate({ scrollTop: 0 }, "slow");
		});
		
		checkToTop();

		function checkToTop() {
			if ( $('html,body').scrollTop() < 150 ) {
				top.addClass('hide');
			}
			else {
				top.removeClass('hide');
			}
		}
	}());

	// Ajax load
	(function(){
		var loadMore = $('.to-load-more'),
			content = $('#products-container'),
			sorting = $('.to-sorting:checked'),
			perPage = 6,
			pageCount = 1;

		loadAjax(1, perPage, sorting.val());

		$('.to-sorting').click(function(){
			if ( $(this).val() !== sorting.val() ) {
				sorting = $(this);
				content.html('');
				loadMore.show();
				pageCount = 1;
				loadAjax(pageCount, perPage, $(this).val());
			}
		});

		loadMore.click(function(){
			loadAjax(++pageCount, perPage, sorting.val());
		});

		function loadAjax(page, perPage, orderBy) {
			$.ajax({
				type: "POST",
				url: 'loadmore.php',
				data: {
					page: page,
					perPage: perPage,
					orderBy: orderBy
				},
				success: function(data){
					content.append(data);
					$.ajax({
						type: "POST",
						url: 'loadmore.php',
						data: {
							page: page + 1,
							perPage: perPage,
							orderBy: orderBy
						},
						success: function(data){
							if ( !data ) {
								loadMore.hide();
							}
						}
					});

				}
			});
		}

	}());
});

// Form validation (email)
function validation(th) {
	var email = document.getElementById('email').value,
	emailPattern = /[0-9a-z_-]+@[0-9a-z_-]+\.[a-z]{2,5}/i,
	msgError = '';
	if ( !emailPattern.test(email) ) {
		msgError += 'Не коректный email ';
	}

	if ( msgError ) {
		alert(msgError);
	}
	else {
		alert('Вы успешно подписались!');
		var inputs = $(th).find('input');
		inputs.val('');
	}
}